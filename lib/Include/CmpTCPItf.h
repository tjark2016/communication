 /**
 * <interfacename>CmpTCP</interfacename>
 * <description></description>
 *
 * <copyright></copyright>
 */


	
	
#ifndef _CMPTCPITF_H_
#define _CMPTCPITF_H_

#include "CmpStd.h"

 

 




/** EXTERN LIB SECTION BEGIN **/
/*  Comments are ignored for m4 compiler so restructured text can be used.  */

#ifdef __cplusplus
extern "C" {
#endif

/**
 *{attribute 'conditionalshow'}
 *{attribute 'conditionalshow' := 'SomeText'}
 *{attribute 'hide'}
 */
#define ERROR_NO_ERROR    0	
#define ERROR_TIME_OUT    1	
/* Typed enum definition */
#define ERROR    RTS_IEC_INT

/**
 * <description>close_sock</description>
 */
typedef struct tagclose_sock_struct
{
	RTS_IEC_INT sock;					/* VAR_INPUT */	
	RTS_IEC_INT close_sock;				/* VAR_OUTPUT */	
} close_sock_struct;

void CDECL CDECL_EXT close_sock(close_sock_struct *p);
typedef void (CDECL CDECL_EXT* PFCLOSE_SOCK_IEC) (close_sock_struct *p);
#if defined(CMPTCP_NOTIMPLEMENTED) || defined(CLOSE_SOCK_NOTIMPLEMENTED)
	#define USE_close_sock
	#define EXT_close_sock
	#define GET_close_sock(fl)  ERR_NOTIMPLEMENTED
	#define CAL_close_sock(p0) 
	#define CHK_close_sock  FALSE
	#define EXP_close_sock  ERR_OK
#elif defined(STATIC_LINK)
	#define USE_close_sock
	#define EXT_close_sock
	#define GET_close_sock(fl)  CAL_CMGETAPI( "close_sock" ) 
	#define CAL_close_sock  close_sock
	#define CHK_close_sock  TRUE
	#define EXP_close_sock  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"close_sock", (RTS_UINTPTR)close_sock, 1, 0x000CDA40, 0x0305081E) 
#elif defined(MIXED_LINK) && !defined(CMPTCP_EXTERNAL)
	#define USE_close_sock
	#define EXT_close_sock
	#define GET_close_sock(fl)  CAL_CMGETAPI( "close_sock" ) 
	#define CAL_close_sock  close_sock
	#define CHK_close_sock  TRUE
	#define EXP_close_sock  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"close_sock", (RTS_UINTPTR)close_sock, 1, 0x000CDA40, 0x0305081E) 
#elif defined(CPLUSPLUS_ONLY)
	#define USE_CmpTCPclose_sock
	#define EXT_CmpTCPclose_sock
	#define GET_CmpTCPclose_sock  ERR_OK
	#define CAL_CmpTCPclose_sock  close_sock
	#define CHK_CmpTCPclose_sock  TRUE
	#define EXP_CmpTCPclose_sock  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"close_sock", (RTS_UINTPTR)close_sock, 1, 0x000CDA40, 0x0305081E) 
#elif defined(CPLUSPLUS)
	#define USE_close_sock
	#define EXT_close_sock
	#define GET_close_sock(fl)  CAL_CMGETAPI( "close_sock" ) 
	#define CAL_close_sock  close_sock
	#define CHK_close_sock  TRUE
	#define EXP_close_sock  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"close_sock", (RTS_UINTPTR)close_sock, 1, 0x000CDA40, 0x0305081E) 
#else /* DYNAMIC_LINK */
	#define USE_close_sock  PFCLOSE_SOCK_IEC pfclose_sock;
	#define EXT_close_sock  extern PFCLOSE_SOCK_IEC pfclose_sock;
	#define GET_close_sock(fl)  s_pfCMGetAPI2( "close_sock", (RTS_VOID_FCTPTR *)&pfclose_sock, (fl) | CM_IMPORT_EXTERNAL_LIB_FUNCTION, 0x000CDA40, 0x0305081E)
	#define CAL_close_sock  pfclose_sock
	#define CHK_close_sock  (pfclose_sock != NULL)
	#define EXP_close_sock   s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"close_sock", (RTS_UINTPTR)close_sock, 1, 0x000CDA40, 0x0305081E) 
#endif


/**
 * <description>init_server</description>
 */
typedef struct taginit_server_struct
{
	RTS_IEC_STRING address[81];			/* VAR_INPUT */	
	RTS_IEC_INT port;					/* VAR_INPUT */	
	RTS_IEC_INT init_server;			/* VAR_OUTPUT */	
} init_server_struct;

void CDECL CDECL_EXT init_server(init_server_struct *p);
typedef void (CDECL CDECL_EXT* PFINIT_SERVER_IEC) (init_server_struct *p);
#if defined(CMPTCP_NOTIMPLEMENTED) || defined(INIT_SERVER_NOTIMPLEMENTED)
	#define USE_init_server
	#define EXT_init_server
	#define GET_init_server(fl)  ERR_NOTIMPLEMENTED
	#define CAL_init_server(p0) 
	#define CHK_init_server  FALSE
	#define EXP_init_server  ERR_OK
#elif defined(STATIC_LINK)
	#define USE_init_server
	#define EXT_init_server
	#define GET_init_server(fl)  CAL_CMGETAPI( "init_server" ) 
	#define CAL_init_server  init_server
	#define CHK_init_server  TRUE
	#define EXP_init_server  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"init_server", (RTS_UINTPTR)init_server, 1, 0xFE23D3E5, 0x0305081E) 
#elif defined(MIXED_LINK) && !defined(CMPTCP_EXTERNAL)
	#define USE_init_server
	#define EXT_init_server
	#define GET_init_server(fl)  CAL_CMGETAPI( "init_server" ) 
	#define CAL_init_server  init_server
	#define CHK_init_server  TRUE
	#define EXP_init_server  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"init_server", (RTS_UINTPTR)init_server, 1, 0xFE23D3E5, 0x0305081E) 
#elif defined(CPLUSPLUS_ONLY)
	#define USE_CmpTCPinit_server
	#define EXT_CmpTCPinit_server
	#define GET_CmpTCPinit_server  ERR_OK
	#define CAL_CmpTCPinit_server  init_server
	#define CHK_CmpTCPinit_server  TRUE
	#define EXP_CmpTCPinit_server  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"init_server", (RTS_UINTPTR)init_server, 1, 0xFE23D3E5, 0x0305081E) 
#elif defined(CPLUSPLUS)
	#define USE_init_server
	#define EXT_init_server
	#define GET_init_server(fl)  CAL_CMGETAPI( "init_server" ) 
	#define CAL_init_server  init_server
	#define CHK_init_server  TRUE
	#define EXP_init_server  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"init_server", (RTS_UINTPTR)init_server, 1, 0xFE23D3E5, 0x0305081E) 
#else /* DYNAMIC_LINK */
	#define USE_init_server  PFINIT_SERVER_IEC pfinit_server;
	#define EXT_init_server  extern PFINIT_SERVER_IEC pfinit_server;
	#define GET_init_server(fl)  s_pfCMGetAPI2( "init_server", (RTS_VOID_FCTPTR *)&pfinit_server, (fl) | CM_IMPORT_EXTERNAL_LIB_FUNCTION, 0xFE23D3E5, 0x0305081E)
	#define CAL_init_server  pfinit_server
	#define CHK_init_server  (pfinit_server != NULL)
	#define EXP_init_server   s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"init_server", (RTS_UINTPTR)init_server, 1, 0xFE23D3E5, 0x0305081E) 
#endif


/**
 * <description>sending_message</description>
 */
typedef struct tagsending_message_struct
{
	RTS_IEC_INT client_sock;			/* VAR_INPUT */	
	RTS_IEC_STRING msg[81];				/* VAR_INPUT */	
	RTS_IEC_INT msg_size;				/* VAR_INPUT */	
	RTS_IEC_INT sending_message;		/* VAR_OUTPUT */	
} sending_message_struct;

void CDECL CDECL_EXT sending_message(sending_message_struct *p);
typedef void (CDECL CDECL_EXT* PFSENDING_MESSAGE_IEC) (sending_message_struct *p);
#if defined(CMPTCP_NOTIMPLEMENTED) || defined(SENDING_MESSAGE_NOTIMPLEMENTED)
	#define USE_sending_message
	#define EXT_sending_message
	#define GET_sending_message(fl)  ERR_NOTIMPLEMENTED
	#define CAL_sending_message(p0) 
	#define CHK_sending_message  FALSE
	#define EXP_sending_message  ERR_OK
#elif defined(STATIC_LINK)
	#define USE_sending_message
	#define EXT_sending_message
	#define GET_sending_message(fl)  CAL_CMGETAPI( "sending_message" ) 
	#define CAL_sending_message  sending_message
	#define CHK_sending_message  TRUE
	#define EXP_sending_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"sending_message", (RTS_UINTPTR)sending_message, 1, 0x9A106A88, 0x0305081E) 
#elif defined(MIXED_LINK) && !defined(CMPTCP_EXTERNAL)
	#define USE_sending_message
	#define EXT_sending_message
	#define GET_sending_message(fl)  CAL_CMGETAPI( "sending_message" ) 
	#define CAL_sending_message  sending_message
	#define CHK_sending_message  TRUE
	#define EXP_sending_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"sending_message", (RTS_UINTPTR)sending_message, 1, 0x9A106A88, 0x0305081E) 
#elif defined(CPLUSPLUS_ONLY)
	#define USE_CmpTCPsending_message
	#define EXT_CmpTCPsending_message
	#define GET_CmpTCPsending_message  ERR_OK
	#define CAL_CmpTCPsending_message  sending_message
	#define CHK_CmpTCPsending_message  TRUE
	#define EXP_CmpTCPsending_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"sending_message", (RTS_UINTPTR)sending_message, 1, 0x9A106A88, 0x0305081E) 
#elif defined(CPLUSPLUS)
	#define USE_sending_message
	#define EXT_sending_message
	#define GET_sending_message(fl)  CAL_CMGETAPI( "sending_message" ) 
	#define CAL_sending_message  sending_message
	#define CHK_sending_message  TRUE
	#define EXP_sending_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"sending_message", (RTS_UINTPTR)sending_message, 1, 0x9A106A88, 0x0305081E) 
#else /* DYNAMIC_LINK */
	#define USE_sending_message  PFSENDING_MESSAGE_IEC pfsending_message;
	#define EXT_sending_message  extern PFSENDING_MESSAGE_IEC pfsending_message;
	#define GET_sending_message(fl)  s_pfCMGetAPI2( "sending_message", (RTS_VOID_FCTPTR *)&pfsending_message, (fl) | CM_IMPORT_EXTERNAL_LIB_FUNCTION, 0x9A106A88, 0x0305081E)
	#define CAL_sending_message  pfsending_message
	#define CHK_sending_message  (pfsending_message != NULL)
	#define EXP_sending_message   s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"sending_message", (RTS_UINTPTR)sending_message, 1, 0x9A106A88, 0x0305081E) 
#endif


/**
 * <description>waiting_message</description>
 */
typedef struct tagwaiting_message_struct
{
	RTS_IEC_INT server_sock;			/* VAR_INPUT */	
	RTS_IEC_STRING msg[81];				/* VAR_INPUT */	
	RTS_IEC_INT msg_size;				/* VAR_INPUT */	
	RTS_IEC_INT waiting_message;		/* VAR_OUTPUT */	
} waiting_message_struct;

void CDECL CDECL_EXT waiting_message(waiting_message_struct *p);
typedef void (CDECL CDECL_EXT* PFWAITING_MESSAGE_IEC) (waiting_message_struct *p);
#if defined(CMPTCP_NOTIMPLEMENTED) || defined(WAITING_MESSAGE_NOTIMPLEMENTED)
	#define USE_waiting_message
	#define EXT_waiting_message
	#define GET_waiting_message(fl)  ERR_NOTIMPLEMENTED
	#define CAL_waiting_message(p0) 
	#define CHK_waiting_message  FALSE
	#define EXP_waiting_message  ERR_OK
#elif defined(STATIC_LINK)
	#define USE_waiting_message
	#define EXT_waiting_message
	#define GET_waiting_message(fl)  CAL_CMGETAPI( "waiting_message" ) 
	#define CAL_waiting_message  waiting_message
	#define CHK_waiting_message  TRUE
	#define EXP_waiting_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"waiting_message", (RTS_UINTPTR)waiting_message, 1, 0x29C2624C, 0x0305081E) 
#elif defined(MIXED_LINK) && !defined(CMPTCP_EXTERNAL)
	#define USE_waiting_message
	#define EXT_waiting_message
	#define GET_waiting_message(fl)  CAL_CMGETAPI( "waiting_message" ) 
	#define CAL_waiting_message  waiting_message
	#define CHK_waiting_message  TRUE
	#define EXP_waiting_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"waiting_message", (RTS_UINTPTR)waiting_message, 1, 0x29C2624C, 0x0305081E) 
#elif defined(CPLUSPLUS_ONLY)
	#define USE_CmpTCPwaiting_message
	#define EXT_CmpTCPwaiting_message
	#define GET_CmpTCPwaiting_message  ERR_OK
	#define CAL_CmpTCPwaiting_message  waiting_message
	#define CHK_CmpTCPwaiting_message  TRUE
	#define EXP_CmpTCPwaiting_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"waiting_message", (RTS_UINTPTR)waiting_message, 1, 0x29C2624C, 0x0305081E) 
#elif defined(CPLUSPLUS)
	#define USE_waiting_message
	#define EXT_waiting_message
	#define GET_waiting_message(fl)  CAL_CMGETAPI( "waiting_message" ) 
	#define CAL_waiting_message  waiting_message
	#define CHK_waiting_message  TRUE
	#define EXP_waiting_message  s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"waiting_message", (RTS_UINTPTR)waiting_message, 1, 0x29C2624C, 0x0305081E) 
#else /* DYNAMIC_LINK */
	#define USE_waiting_message  PFWAITING_MESSAGE_IEC pfwaiting_message;
	#define EXT_waiting_message  extern PFWAITING_MESSAGE_IEC pfwaiting_message;
	#define GET_waiting_message(fl)  s_pfCMGetAPI2( "waiting_message", (RTS_VOID_FCTPTR *)&pfwaiting_message, (fl) | CM_IMPORT_EXTERNAL_LIB_FUNCTION, 0x29C2624C, 0x0305081E)
	#define CAL_waiting_message  pfwaiting_message
	#define CHK_waiting_message  (pfwaiting_message != NULL)
	#define EXP_waiting_message   s_pfCMRegisterAPI2( (const CMP_EXT_FUNCTION_REF*)"waiting_message", (RTS_UINTPTR)waiting_message, 1, 0x29C2624C, 0x0305081E) 
#endif


#ifdef __cplusplus
}
#endif

/** EXTERN LIB SECTION END **/




typedef struct
{
	IBase_C *pBase;
} ICmpTCP_C;

#ifdef CPLUSPLUS
class ICmpTCP : public IBase
{
	public:
};
	#ifndef ITF_CmpTCP
		#define ITF_CmpTCP static ICmpTCP *pICmpTCP = NULL;
	#endif
	#define EXTITF_CmpTCP
#else	/*CPLUSPLUS*/
	typedef ICmpTCP_C		ICmpTCP;
	#ifndef ITF_CmpTCP
		#define ITF_CmpTCP
	#endif
	#define EXTITF_CmpTCP
#endif

#ifdef CPLUSPLUS_ONLY
  #undef CPLUSPLUS_ONLY
#endif

#endif /*_CMPTCPITF_H_*/
