/* A simple server in the internet domain using TCP
 The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg) {
  perror(msg);
  exit(1);
}

int init_server(char *address, int port) {
  int server_sock;
  int z;
  struct sockaddr_in server_addr;
  printf("\nWelcome to tcp server!\n");
  //get the server socket
  server_sock = socket(PF_INET, SOCK_STREAM, 0);
  if (server_sock == -1) {
    error("socket() failed\n");
    return server_sock;
  } else
    printf("Socket created!\n");

  //configure server address,port
  bzero((char *) &server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  //server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_addr.s_addr = inet_addr(address);
  if (server_addr.sin_addr.s_addr == INADDR_NONE) {
    error("INADDR_NONE");
    return -1;
  }

  //bind
  z = bind(server_sock, (struct sockaddr *) &server_addr, sizeof server_addr);
  if (z == -1) {
    error("bind() failed\n");
    return -1;
  } else
    printf("Bind Ok!\n");

  //listen
  z = listen(server_sock, 5);
  if (z < 0) {
    printf("Server Listen Failed!");
    return -1;
  } else
    printf("listening...\n");
  return server_sock;
}

int waiting_message(int server_sock, char * msg, int msg_size) {
  socklen_t clilen;
  struct sockaddr_in cli_addr;
  clilen = sizeof(cli_addr);
  int newsockfd;
  newsockfd = accept(server_sock, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0) {
    error("ERROR on accept\n");
    return -1;
  }
  bzero(msg, msg_size);
  int n;
  n = read(newsockfd, msg, msg_size - 1);
  if (n < 0) {
    error("ERROR reading from socket\n");
    return -1;
  }
  printf("Here is the message: %s\n", msg);
  return newsockfd;
}

int sending_message(int client_sock, char * msg, int msg_size) {
  int n;
  n = write(client_sock, msg, msg_size);
  if (n < 0) {
    error("ERROR sending to socket\n");
    return -1;
  }
  return 0;
}

void close_sock(int sock) {
  close(sock);
}

int main(int argc, char *argv[]) {

  int server_sock;
  server_sock = init_server("127.0.0.1", 8888);

  int client_sock;
  char msg[256];
  client_sock = waiting_message(server_sock, msg, 256);

  int res;
  res = sending_message(client_sock, "200", 3);

  close_sock(client_sock);
  close_sock(server_sock);
  return 0;
}
