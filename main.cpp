#include <iostream>

#include "CmpTCPItf.h"

int main(int argc, char* argv[]) {
  init_server_struct iss;
  strcpy(iss.address, "127.0.0.1");
  iss.port = 8888;
  init_server(&iss);
  return 0;
}
